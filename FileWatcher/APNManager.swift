//
//  APNManager.swift
//  FileWatcher
//
//  Created by ALEXANDER CHANCHIKOV on 18.04.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import UIKit
import UserNotifications

class APNManager: NSObject {
  
  static let shared = APNManager()
  
  func registerNotification(complete: @escaping () -> ()) {
    
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {(accepted, error) in
      if !accepted {
        
        print("Notification access denied.")
      }else{
        UIApplication.shared.registerForRemoteNotifications()
      }
      complete()
    }
  }
  
  func sendLocalAPNWithText(_ text : String) {
    
    let content = UNMutableNotificationContent()
    content.title = ""
    content.body = text
    
    let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
    
    let request = UNNotificationRequest(identifier: "Push", content: content, trigger: trigger)
    UNUserNotificationCenter.current().add(request) { (error) in
      
      if error != nil {
        print(error! as NSError)
      }
    }
  }
}


