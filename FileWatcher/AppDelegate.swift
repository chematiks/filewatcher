//
//  AppDelegate.swift
//  FileWatcher
//
//  Created by ALEXANDER CHANCHIKOV on 18.04.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  var bgTask : UIBackgroundTaskIdentifier?
  var timer : Timer?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.

    APNManager.shared.registerNotification {
      
    }

    return true
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    
  }

  func applicationDidEnterBackground(_ application: UIApplication) {

    bgTask = UIApplication.shared.beginBackgroundTask(withName: "CheckChangesInDirectory") {
      
      Watcher.shared.src?.activate()
      UIApplication.shared.endBackgroundTask(self.bgTask!)
      self.bgTask = UIBackgroundTaskInvalid
    }

    timer = Timer.init(timeInterval: 5.0, target: self, selector: #selector(self.checkChangesInDirectory), userInfo: nil, repeats: true)
    RunLoop.main.add(timer!, forMode: .commonModes)
    timer?.fire()
  }
  
  func checkChangesInDirectory() {
    
    Watcher.shared.src?.activate()
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }
  

}

