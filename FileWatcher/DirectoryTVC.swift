//
//  DirectoryTVC.swift
//  FileWatcher
//
//  Created by ALEXANDER CHANCHIKOV on 18.04.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import UIKit

struct FolderData {
  var content : [String]
  var path : String
}

class DirectoryTVC: UITableViewController {

  var objects : Array<String> = []
  
  var path : String!
  
  var pullToRefreshControl : UIRefreshControl!
  
  override func viewDidLoad() {
    
    super.viewDidLoad()
    
    let pullToRefresh = UIRefreshControl.init(frame: CGRect.init(x: 0,
                                                                 y: 0,
                                                                 width: UIScreen.main.bounds.width,
                                                                 height: 50.0))
    pullToRefresh.attributedTitle = NSAttributedString.init(string: "Pull-to-Refresh")
    pullToRefresh.addTarget(self,
                            action: #selector(self.loadAndShowContent),
                            for: .valueChanged)
    pullToRefreshControl = pullToRefresh
    tableView.addSubview(pullToRefresh)
  
    if self.navigationController?.viewControllers.first == self {
      
      let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                      .userDomainMask,
                                                      true)
      self.path = paths.first

      objects = self.loadContentFromPath(self.path)

      self.showAlert("Warning", message: "Please select file or directories for watching")
      
     let watchFolder = UIBarButtonItem(title: "Watch",
                                        style: .plain,
                                        target: self,
                                        action: #selector(self.beginWatchCurrentDirectory))
      self.navigationItem.leftBarButtonItem = watchFolder

    }else{
      let newBackButton = UIBarButtonItem(title: "Back",
                                          style: .plain,
                                          target: self, action: #selector(self.back(_:)))
      self.navigationItem.leftBarButtonItem = newBackButton
    }
  }
  
  func beginWatchCurrentDirectory() {
    
    Watcher.shared.beginWatchForPath(self.path)
  }


  func loadAndShowContent() {
    
    self.objects = self.loadContentFromPath(self.path)
    self.tableView.reloadData()
    self.pullToRefreshControl.endRefreshing()
  }
  
  func loadContentFromPath(_ path : String) -> Array<String> {
    
    do {
      let content = try FileManager.default.contentsOfDirectory(atPath: path)
      return content
    } catch let error as NSError {
      print(error)
      self.showError(error)
      return []
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.title = self.path?.lastPathComponent
  }
  
  @IBAction func back(_ sender: UIButton) {
    
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return objects.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cellID",
                                             for: indexPath) as! FolderTableViewCell
    
    cell.folderNameLabel?.text = objects[indexPath.row]
    cell.delegate = self
    return cell
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "showFolderSegueID" {
      
      let destinationTVC = segue.destination as! DirectoryTVC
      
      if let folderData = sender as? FolderData {
        destinationTVC.objects = folderData.content
        destinationTVC.path = folderData.path
        print("TODO: segue showFolderSegueID")
      }
    }
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let folder = objects[indexPath.row]
    let nextPath = self.path.stringByAppendingPathComponent(path: folder)
    
    let content = self.loadContentFromPath(nextPath)
    self.performSegue(withIdentifier: "showFolderSegueID", sender: FolderData.init(content: content, path: nextPath))
    
    tableView.deselectRow(at: indexPath, animated: true)
  }

}

extension DirectoryTVC : FolderTableViewCellDelegate {
  
  func watchButtonPressInCell(_ cell : FolderTableViewCell) {
    
    if let indexPath = self.tableView.indexPath(for: cell) {

      let folderName = objects[indexPath.row]
      let nextPath = self.path.stringByAppendingPathComponent(path: folderName)

      Watcher.shared.beginWatchForPath(nextPath)
      
    }
  }
}

