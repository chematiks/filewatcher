//
//  FolderTableViewCell.swift
//  FileWatcher
//
//  Created by ALEXANDER CHANCHIKOV on 18.04.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import UIKit

protocol FolderTableViewCellDelegate {
  func watchButtonPressInCell(_ cell : FolderTableViewCell)
}

class FolderTableViewCell: UITableViewCell {

  var delegate : FolderTableViewCellDelegate?

  @IBOutlet weak var folderNameLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  @IBAction func watchButtonPress(_ sender: Any) {
    
    self.delegate?.watchButtonPressInCell(self)
  }
}



