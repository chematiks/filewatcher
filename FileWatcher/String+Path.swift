//
//  String+Path.swift
//  FileObserver
//
//  Created by ALEXANDER CHANCHIKOV on 13.04.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import Foundation

extension String {
  
  var lastPathComponent: String {
    return (self as NSString).lastPathComponent
  }
  var pathExtension: String {
    return (self as NSString).pathExtension
  }
  var stringByDeletingLastPathComponent: String {
    return (self as NSString).deletingLastPathComponent
  }
  var stringByDeletingPathExtension: String {
    return (self as NSString).deletingPathExtension
  }
  var pathComponents: [String] {
    return (self as NSString).pathComponents
  }
  func stringByAppendingPathComponent(path: String) -> String {
    let nsSt = self as NSString
    return nsSt.appendingPathComponent(path)
  }
  func stringByAppendingPathExtension(ext: String) -> String? {
    let nsSt = self as NSString
    return nsSt.appendingPathExtension(ext)
  }
}
