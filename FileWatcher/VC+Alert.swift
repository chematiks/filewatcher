//
//  VC+Alert.swift
//  FileWatcher
//
//  Created by ALEXANDER CHANCHIKOV on 18.04.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import UIKit

extension UIViewController {
  
  func showError(_ error : Error) {
    
    self.showAlert( "Error", message: error.localizedDescription)
  }
  
  func showAlert(_ title : String, message : String) {
    
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert) -> Void in
      
    }))
    self.present(alert, animated: true, completion: nil)
  }
    
  func showAlert(_ message : String) {
    
    let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK" , style: .default, handler: { (alert) -> Void in
      
    }))
    self.present(alert, animated: true, completion: nil)
    
  }
}
