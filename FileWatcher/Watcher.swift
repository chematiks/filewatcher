//
//  Watcher.swift
//  FileWatcher
//
//  Created by ALEXANDER CHANCHIKOV on 18.04.17.
//  Copyright © 2017 ALEXANDER CHANCHIKOV. All rights reserved.
//

import UIKit
import Foundation

class Watcher: NSObject {
  
  static let shared = Watcher()

  var src : DispatchSourceFileSystemObject?
  
  private var path : String?
  private var contentInSelectDirectory : Array<String> = []
  
  func beginWatchForPath(_ path : String) {
    
    if src != nil {
      self.endWatching()
    }
    
    let fileDescriptor = open(NSString.init(string: path).fileSystemRepresentation, O_EVTONLY)
    self.path = path
    contentInSelectDirectory = self.loadContentFromSelectDirectory()
    
    let queue = DispatchQueue.global()
  
    src = DispatchSource.makeFileSystemObjectSource(fileDescriptor: fileDescriptor, eventMask: .write, queue: queue)
    
    if src != nil {
      src?.setEventHandler {
        print("changes")
        
        let currentContentInDirectory = self.loadContentFromSelectDirectory()
        
        var pushText = ""
        
        if self.contentInSelectDirectory.count == currentContentInDirectory.count {
          
          var fileNameIsChanged = ""
          for fileName in self.contentInSelectDirectory {
            if currentContentInDirectory.contains(fileName) == false {
              fileNameIsChanged = fileName
              break
            }
          }
          if pushText.characters.count > 0 {
            pushText = "In folder \"\((self.path?.lastPathComponent)!)\" change file \"\(fileNameIsChanged)\""
          }else{
            pushText = "In folder \"\((self.path?.lastPathComponent)!)\" change file"
          }
        }else{
          
          if self.contentInSelectDirectory.count > currentContentInDirectory.count {
           
            var fileNameIsRemoved = ""
            for fileName in self.contentInSelectDirectory {
              if currentContentInDirectory.contains(fileName) == false {
                fileNameIsRemoved = fileName
                break
              }
            }
            pushText = "From folder \"\((self.path?.lastPathComponent)!)\" remove file \"\(fileNameIsRemoved)\""
          }else{

            var fileNameIsAdded = ""
            for fileName in currentContentInDirectory {
              if self.contentInSelectDirectory.contains(fileName) == false {
                fileNameIsAdded = fileName
                break
              }
            }
            pushText = "In folder \"\((self.path?.lastPathComponent)!)\" add file \"\(fileNameIsAdded)\""
          }
        }
        self.contentInSelectDirectory = currentContentInDirectory
        APNManager.shared.sendLocalAPNWithText(pushText)
      }
      src?.setCancelHandler {
        print("cancel")
      }
    
      src?.resume()
    }
  }
  
  private func loadContentFromSelectDirectory() -> Array<String> {
    
    if self.path == "" {
      return []
    }else{
      do {
        return try FileManager.default.contentsOfDirectory(atPath: self.path!)
      } catch let error as NSError {
        print(error)
        return []
      }
    }
  }
  
  private func endWatching() {
    
    if src != nil {
      src?.cancel()
      src = nil
      contentInSelectDirectory = []
    }
  }
  
}
